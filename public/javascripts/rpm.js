const buildRpm = require('rpm-builder'); // eslint-disable-line import/no-extraneous-dependencies
const app = require('../../package');

const options = {
    execOpts: {
        maxBuffer: 1024 * 1024 * 10
    },
    name: `RELIRK${app.name}`,
    version: app.version,
    release: '1',
    buildArch: 'noarch',
    files: [{ cwd: 'dist', src: '*', dest: `/opt/${app.name}` }],
    excludeFiles: ['rpm.js', 'test/', '*.tgz', '*.rpm']
};

buildRpm(options, (err) => {
    if (err) {
        throw err
    }
});
