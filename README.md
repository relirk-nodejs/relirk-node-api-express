![image](public/images/relirk-logo-beta-0.0.1.png)

## Instalação das dependências
#
    npm install
#

## Inicializando a aplicação com a configuração de DESENVOLVIMENTO
#
    npm run dev
#

## Inicializando a aplicação com a configuração de PRODUÇÃO
#
    npm start
#

As especificações de configuração para cad ambiente se encontram nos arquivos .env e .env.development respectivamente

## Testes
#
    npm run unit // Testes unitários
    npm run integration // Testes de integração
#
