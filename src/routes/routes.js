import express from "express";
import IndexController from "../controllers/IndexController";
import ProductController from "../controllers/ProductController";

const routes = express.Router();

routes.get('/', IndexController.start);
routes.get('/products', ProductController.index);
routes.get('/products/:id', ProductController.show);

routes.post('/products', ProductController.store);
routes.post('/products/:id', ProductController.upsert); // Faltando testes

routes.put('/products/:id', ProductController.update);

routes.delete('/products/:id', ProductController.destroy);

module.exports = routes;
