import requireDir from "require-dir";
import cookieParser from "cookie-parser";
import createError from "http-errors";
import cors from "cors";
import morgan from "morgan";
import mongoose from "mongoose";
import path from "path";
import express from "express";

// Initialize MongoDB
mongoose.connect(
    'mongodb://localhost:27017/node-api',
    {useNewUrlParser: true, useCreateIndex: true}
).then(() => {
    console.log('Connection with MongoDB successfully established. Ready to use!')
}).catch((e) => {
    throw `Error connecting to MongoDB! {error: ${e}}`
});

const PORT = 3000;
const HOST = "0.0.0.0";
const app = express();

app.use(cors());
app.use(morgan('dev'));
app.use(morgan(':method :url :response-time'));
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

//Discover all mongodb models
requireDir('models');

// Routes
app.use('/api', require('./routes/routes'));

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// Error handler
app.use( (err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.render('error');
});

app.listen(PORT, HOST, () => {
   console.log(`Started on port ${PORT}`)
});

export default app;
