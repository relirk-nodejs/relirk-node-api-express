import mongoose from "mongoose";

const Product = mongoose.model('Product');

export default {
    async index(req, res) {
        try {
            const {page = 1} = req.query;
            const products = await Product.paginate({}, {page, limit: 10});
            // const products = await Product.find(); // Find sem paginação
            return res.json(products);
        } catch (e) {
            throw e
        }
    },

    async store(req, res) {
        try {
            let product = await Product.create(req.body);
            return res.json(product);
        } catch (e) {
            throw e
        }
    },

    async show(req, res) {
        try {
            const product = await Product.findById(req.params.id);
            return res.json(product);
        } catch (e) {
            throw e
        }
    },

    async update(req, res) {
        try {
            const product = await Product.findByIdAndUpdate(req.params.id, req.body, {new: true});
            return res.json(product);
        } catch (e) {
            throw e
        }
    },

    async destroy(req, res) {
        await Product.findByIdAndRemove(req.params.id);
        return res.send()
    },

    async upsert(req, res) {
        try {
            let query = {'_id': req.params.id};
            const product = await Product.findOneAndUpdate(query, req.body, {upsert: true});
            return res.json(product);
        } catch (e) {
            throw e
        }
    }
};

