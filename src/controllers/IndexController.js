import defs from "../../package";

const start = (req, res) => {
    return res.render('index.js', {
        title: 'Relirk',
        description: 'node-api',
        version: `${defs.version}`
    });
};

export default {
  start
};
