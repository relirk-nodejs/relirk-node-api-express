FROM node:10-alpine

# Create app directory
RUN mkdir -p /usr/app
WORKDIR /usr/app

# Install app dependencies
COPY package.json package-lock.json yarn.lock /urs/app/
RUN yarn install
RUN npm install

# Bundle app source
COPY . /usr/app

EXPOSE 3000
CMD ["yarn", "start"]

